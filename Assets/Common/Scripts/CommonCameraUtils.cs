﻿using UnityEngine;

namespace Common.Scripts
{
    public class CommonCameraUtils : MonoBehaviour
    {
        public static Vector3 GetScrollVector(Vector3 mousePosition)
        {
            Vector3 camMoveVector = Vector3.zero;

            if (mousePosition.x < 20 && mousePosition.x > 0)
                camMoveVector.x = -1;

            if (mousePosition.x > Screen.width - 20 && mousePosition.x < Screen.width)
                camMoveVector.x = 1;

            if (mousePosition.y < 20 && mousePosition.y > 0)
                camMoveVector.y = -1;

            if (mousePosition.y > Screen.height - 20 && mousePosition.y < Screen.height)
                camMoveVector.y = 1;
			
            return camMoveVector;
        }
    }
}