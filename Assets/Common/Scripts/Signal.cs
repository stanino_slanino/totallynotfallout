using System;
using System.Collections.Generic;

namespace Common.Scripts
{
    public class Signal<TPayload1, TPayload2, TPayload3, TPayload4>
    {
        #region Fields

        private List<Action<TPayload1, TPayload2, TPayload3, TPayload4>> m_listeners;
        private List<bool> m_once;
        #endregion Fields

        #region Constructors

        public Signal()
        {
            m_listeners = new List<Action<TPayload1, TPayload2, TPayload3, TPayload4>>();
            m_once = new List<bool>();
        }

        #endregion Constructors

        #region Methods

        public void Add(Action<TPayload1, TPayload2, TPayload3, TPayload4> listener)
        {
            m_listeners.Add(listener);
            m_once.Add(false);
        }

        public void AddOnce(Action<TPayload1, TPayload2, TPayload3, TPayload4> listener)
        {
            m_once.Add(true);
            m_listeners.Add(listener);
        }

        public void Remove(Action<TPayload1, TPayload2, TPayload3, TPayload4> listener)
        {
            int i = m_listeners.IndexOf(listener);

            if (i >= 0)
            {
                m_listeners.RemoveAt(i);
                m_once.RemoveAt(i);
            }
        }

        public void RemoveAll()
        {
            m_listeners.Clear();
            m_once.Clear();
        }

        public void Dispatch(TPayload1 payload1, TPayload2 payload2, TPayload3 payload3, TPayload4 payload4)
        {
            for (int i = m_listeners.Count - 1; i >= 0; i--)
            {
                Action<TPayload1, TPayload2, TPayload3, TPayload4> action = m_listeners[i];

                if (m_once[i])
                {
                    m_listeners.RemoveAt(i);
                    m_once.RemoveAt(i);
                }

                action.Invoke(payload1, payload2, payload3, payload4);
            }
        }

        #endregion Methods
    }


    public class Signal<TPayload1, TPayload2, TPayload3>
    {
        #region Fields

        private List<Action<TPayload1, TPayload2, TPayload3>> m_listeners;
        private List<bool> m_once;
        #endregion Fields

        #region Constructors

        public Signal()
        {
            m_listeners = new List<Action<TPayload1, TPayload2, TPayload3>>();
            m_once = new List<bool>();
        }

        #endregion Constructors

        #region Methods

        public void Add(Action<TPayload1, TPayload2, TPayload3> listener)
        {
            m_listeners.Add(listener);
            m_once.Add(false);
        }

        public void AddOnce(Action<TPayload1, TPayload2, TPayload3> listener)
        {
            m_once.Add(true);
            m_listeners.Add(listener);
        }

        public void Remove(Action<TPayload1, TPayload2, TPayload3> listener)
        {
            int i = m_listeners.IndexOf(listener);

            if (i >= 0)
            {
                m_listeners.RemoveAt(i);
                m_once.RemoveAt(i);
            }
        }

        public void RemoveAll()
        {
            m_listeners.Clear();
            m_once.Clear();
        }

        public void Dispatch(TPayload1 payload1, TPayload2 payload2, TPayload3 payload3)
        {
            for (int i = m_listeners.Count - 1; i >= 0; i--)
            {
                Action<TPayload1, TPayload2, TPayload3> action = m_listeners[i];

                if (m_once[i])
                {
                    m_listeners.RemoveAt(i);
                    m_once.RemoveAt(i);
                }

                action.Invoke(payload1, payload2, payload3);
            }
        }

        #endregion Methods
    }

    public class Signal<TPayload1, TPayload2>
    {
        #region Fields

        private List<Action<TPayload1, TPayload2>> m_listeners;
        private List<bool> m_once;
        #endregion Fields

        #region Constructors

        public Signal()
        {
            m_listeners = new List<Action<TPayload1, TPayload2>>();
            m_once = new List<bool>();
        }

        #endregion Constructors

        #region Methods

        public void Add(Action<TPayload1, TPayload2> listener)
        {
            m_listeners.Add(listener);
            m_once.Add(false);
        }

        public void AddOnce(Action<TPayload1, TPayload2> listener)
        {
            m_once.Add(true);
            m_listeners.Add(listener);
        }

        public void Remove(Action<TPayload1, TPayload2> listener)
        {
            int i = m_listeners.IndexOf(listener);

            if (i >= 0)
            {
                m_listeners.RemoveAt(i);
                m_once.RemoveAt(i);
            }
        }

        public void RemoveAll()
        {
            m_listeners.Clear();
            m_once.Clear();
        }

        public void Dispatch(TPayload1 payload1, TPayload2 payload2)
        {
            for (int i = m_listeners.Count - 1; i >= 0; i--)
            {
                Action<TPayload1, TPayload2> action = m_listeners[i];

                if (m_once[i])
                {
                    m_listeners.RemoveAt(i);
                    m_once.RemoveAt(i);
                }

                action.Invoke(payload1, payload2);
            }
        }

        #endregion Methods
    }

    public class Signal<TPayload>
    {
        #region Fields

        public List<Action<TPayload>> m_listeners;
        private List<bool> m_once;
        #endregion Fields

        #region Constructors

        public Signal()
        {
            m_listeners = new List<Action<TPayload>>();
            m_once = new List<bool>();
        }

        #endregion Constructors

        #region Methods

        public void Add(Action<TPayload> listener)
        {
            m_listeners.Add(listener);
            m_once.Add(false);
        }

        public void AddOnce(Action<TPayload> listener)
        {
            m_listeners.Add(listener);
            m_once.Add(true);
        }

        public void Remove(Action<TPayload> listener)
        {
            int i = m_listeners.IndexOf(listener);

            if (i >= 0)
            {
                m_listeners.RemoveAt(i);
                m_once.RemoveAt(i);
            }
        }

        public void RemoveAll()
        {
            m_listeners.Clear();
            m_once.Clear();
        }

        public void Dispatch(TPayload payload)
        {
            for (int i = m_listeners.Count - 1; i >= 0; i--)
            {
                Action<TPayload> action = m_listeners[i];

                if (m_once[i])
                {
                    m_listeners.RemoveAt(i);
                    m_once.RemoveAt(i);
                }

                action.Invoke(payload);
            }
        }

        #endregion Methods
    }

    public class Signal
    {
        #region Fields

        private List<Action> m_listeners;
        private List<bool> m_once;
        #endregion Fields

        #region Constructors

        public Signal()
        {
            m_listeners = new List<Action>();
            m_once = new List<bool>();
        }

        #endregion Constructors

        #region Methods

        public void Add(Action listener)
        {
            m_listeners.Add(listener);
            m_once.Add(false);
        }

        public void AddOnce(Action listener)
        {
            m_once.Add(true);
            m_listeners.Add(listener);
        }

        public void Remove(Action listener)
        {
            int i = m_listeners.IndexOf(listener);

            if (i >= 0)
            {
                m_listeners.RemoveAt(i);
                m_once.RemoveAt(i);
            }
        }


        public void RemoveAll()
        {
            m_listeners.Clear();
            m_once.Clear();
        }


        public void Dispatch()
        {
            for (int i = m_listeners.Count - 1; i >= 0; i--)
            {
                Action action = m_listeners[i];

                if (m_once[i])
                {
                    m_listeners.RemoveAt(i);
                    m_once.RemoveAt(i);
                }

                action.Invoke();
            }
        }

        #endregion Methods
    }
}