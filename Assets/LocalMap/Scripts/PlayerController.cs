﻿using System;
using System.Collections.Generic;
using Common.Scripts;
using UnityEngine;

namespace LocalMap.Scripts
{
	public class PlayerController : MonoBehaviour
	{
		public float Speed;
		public Animator _animator;

		private const float height = 1f;
		private Vector3 _destination;
		private TileLight _destinationTile;
		private Ladder _destinationLadder;
		private bool _isMoving;
		private TileSurface _tileSurface;
		private TileLight currentTile;
		private Queue<TileLight> path;

		public TileSurface TileSurface
		{
			get { return _tileSurface; }
			set { _tileSurface = value; }
		}

		private LocalMapEventManager _eventManager;
		
		public Signal<Tile> ArrivedAtDestinationTile = new Signal<Tile>();
		public Signal<Ladder> ArrivedAtDestinationLadder = new Signal<Ladder>();

		// Use this for initialization
		public void OnMapLoaded(TileSurface surface)
		{
			_tileSurface = surface;
			int indexX = _tileSurface.TileLightGrid.GetUpperBound(0) / 2;
			int indexY = _tileSurface.TileLightGrid.GetUpperBound(1) / 2;
			currentTile = _tileSurface.TileLightGrid[indexX, indexY];
			transform.position = currentTile.WorldPosition;
			_destination = transform.position;
			_animator = GetComponent<Animator>();
		}
		
		private void OnDisable()
		{
			_eventManager.OnTileClicked.Remove(GetPathWaypoints);
			_eventManager.OnLadderClicked.Remove(SetDestinationLadder);
		}

		private void SetDestinationLadder(Ladder ladder)
		{
			_destination = ladder.transform.position;
			_destination.y = transform.position.y;
			_destinationLadder = ladder;
			ArrivedAtDestinationLadder.AddOnce(_eventManager.OnPlayerArrivedAtLadder);
		}

		private void GetPathWaypoints(Tile destinationTile)
		{
			path = PathFinder.FindPath(currentTile, destinationTile.TileLight, TileSurface);
			_destinationTile = path.Dequeue();
			_isMoving = true;
			_animator.SetBool("isWalking", true);
		}

		public void SetEventController(LocalMapEventManager em)
		{
			_eventManager = em;
			_eventManager.OnTileClicked.Add(GetPathWaypoints);
			_eventManager.OnLadderClicked.Add(SetDestinationLadder);
		}

		private static bool Check2DPosition(Vector3 firstPos, Vector3 secondPos)
		{
			return Math.Abs(firstPos.x - secondPos.x) < 0.1f && Math.Abs(firstPos.z - secondPos.z) < 0.1f;
		}

		private void Update()
		{
			float step = Speed * Time.deltaTime;
			Vector3 current = Vector3.MoveTowards(transform.position, _destination, step);
			transform.position = current;

			if (_destinationLadder != null)
			{
				if (Check2DPosition(current, _destinationLadder.transform.position))
				{
					ArrivedAtDestinationLadder.Dispatch(_destinationLadder);
					_isMoving = false;
					_animator.SetBool("isWalking", false);
				}
			}

			if (_destinationTile != null)
			{
				if (!Check2DPosition(current, _destinationTile.WorldPosition) || !_isMoving) return;
				if (path.Count == 0)
				{
					_isMoving = false;
					_animator.SetBool("isWalking", false);
					currentTile = _destinationTile;
				}
				else
				{
					_destinationTile = path.Dequeue();
					_destination = _destinationTile.WorldPosition;
					transform.LookAt(_destination);
				}
				ArrivedAtDestinationTile.Dispatch(_destinationTile.Tile);
			}
		}
	}
}
