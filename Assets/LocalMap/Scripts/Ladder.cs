﻿using UnityEngine;
using Common.Scripts;

namespace LocalMap.Scripts
{
	public class Ladder : MonoBehaviour
	{

		public TileSurface SurfaceLower;
		public TileSurface SurfaceUpper;
		public Material ActiveMaterial;
		public Material NormalMaterial;
		
		public Signal<Ladder> OnMouseoverEnterSignal = new Signal<Ladder>();
		public Signal<Ladder> OnMouseoverExitSignal = new Signal<Ladder>();
		public Signal<Ladder> OnMouseClickedSignal = new Signal<Ladder>();

		private bool _isSelected = false;
		private float _timeSinceSelected;
		private LocalMapEventManager _eventManager;
		private MeshRenderer _mesh;

		private void OnEnable()
		{
			_eventManager = FindObjectOfType<LocalMapEventManager>();
			_mesh = GetComponent<MeshRenderer>();
		}
		
		private void OnMouseEnter()
		{
			SetToActive();
			OnMouseoverEnterSignal.Dispatch(this);
		}

		private void OnMouseExit()
		{
			SetToEmpty();
			OnMouseoverExitSignal.Dispatch(this);
		}

		private void OnMouseDown()
		{
			OnMouseClickedSignal.Dispatch(this);
		}

		private void SetToActive()
		{
			_mesh.material = ActiveMaterial;
			_isSelected = true;
			_timeSinceSelected = 0;
		}
		
		private void SetToEmpty()
		{
			_mesh.material = NormalMaterial;
			_isSelected = false;
			_timeSinceSelected = 0;
		}
	}
}
