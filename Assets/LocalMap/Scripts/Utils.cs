﻿using UnityEngine;

namespace LocalMap.Scripts
{
    public static class TileMetrics
    {
        public const float size = 1.0f;
        public static Vector3 PlayerSize = new Vector3(0.45f, 0.9f, 0.45f);
        public static  Vector3 PlayerOffset = new Vector3(0.0f, 1.0f, 0.0f); 
    }
}
