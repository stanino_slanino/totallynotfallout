﻿using System.Collections.Generic;
using EpPathFinding.cs;
using UnityEngine;

namespace LocalMap.Scripts
{
    public static class PathFinder
    {
        public static BaseGrid CreateBaseGrid(TileLight[,] tileGrid)
        {
            int xDimension = tileGrid.GetUpperBound(0);
            int yDimension = tileGrid.GetUpperBound(1);
            List<TileLight> result = new List<TileLight>();
            BaseGrid searchGrid = new StaticGrid(xDimension, yDimension);

            for (int x = 0; x < xDimension; x++)
            {
                for (int y = 0; y < yDimension; y++)
                {
                    if (tileGrid[x, y].IsPassable)
                    {
                        searchGrid.SetWalkableAt(x, y, true);
                    }
                        
                }
            }
            return searchGrid;
        }
        
        public static Queue<TileLight> FindPath(TileLight starTile, TileLight endTile, TileSurface tileSurface)
        {
            Queue<TileLight> result = new Queue<TileLight>();
            BaseGrid baseGrid = CreateBaseGrid(tileSurface.TileLightGrid);
            GridPos startPos = new GridPos(starTile.IndexX, starTile.IndexY);
            GridPos endPos = new GridPos(endTile.IndexX, endTile.IndexY);
            JumpPointParam jumpPointParam = new JumpPointParam(baseGrid, startPos, endPos, EndNodeUnWalkableTreatment.DISALLOW);
           
            List<GridPos> resultNodes = JumpPointFinder.FindPath(jumpPointParam);
            foreach (var node in resultNodes)
            {
                TileLight t = tileSurface.TileLightGrid[node.x, node.y];
                result.Enqueue(t);
            }
            return result;
        }
    }
}
