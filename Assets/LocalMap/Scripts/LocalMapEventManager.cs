﻿using System.Collections.Generic;
using Common.Scripts;
using UnityEngine;

namespace LocalMap.Scripts
{
    public class LocalMapEventManager : MonoBehaviour
    {
        private List<Ladder> _ladderList;
        
    
        public Camera Cam;
        public TileController TileController;
        
        public Signal<Vector3> OnMouseOnBounds = new Signal<Vector3>();
        public Signal<Tile> OnMouseOnHex = new Signal<Tile>();
        public Signal<Tile> OnTileClicked = new Signal<Tile>();
        public Signal<Ladder> OnLadderClicked = new Signal<Ladder>();
        public Signal<Ladder> OnMouseOnLadder = new Signal<Ladder>();

        private PlayerController _player;
        public PlayerController PlayerPrefab;

        private void OnEnable()
        {
            _ladderList = new List<Ladder>(FindObjectsOfType<Ladder>());
            foreach (Ladder ladder in _ladderList)
            {
                ladder.OnMouseClickedSignal.Add(LadderClickedDispatcher);
            }
            
            
            _player = Instantiate(PlayerPrefab, Vector3.zero, Quaternion.identity);
            _player.SetEventController(this);
            _player.OnMapLoaded(TileController.Ground);

        }


        private void LadderClickedDispatcher(Ladder ladder)
        {
            OnLadderClicked.Dispatch(ladder);
        }

        public void TileClickedDispatcher(Tile tile)
        {
            OnTileClicked.Dispatch(tile);
        }

        public void OnPlayerArrivedAtTile(Tile tile)
        {
            Debug.Log(tile);
        }

        public void OnPlayerArrivedAtLadder(Ladder ladder)
        {
            Debug.Log(ladder);
        }

        // Update is called once per frame
        private void Update()
        {
            Vector3 mPosition = Input.mousePosition;
            Vector3 scrollVector = CommonCameraUtils.GetScrollVector(mPosition);

            if (scrollVector != Vector3.zero)
                OnMouseOnBounds.Dispatch(scrollVector);
        }
    }
}