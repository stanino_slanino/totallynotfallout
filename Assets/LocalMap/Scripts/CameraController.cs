﻿using UnityEngine;

namespace LocalMap.Scripts
{
	public class CameraController : MonoBehaviour
	{

		public float ScrollSpeed = 3.0f;

		private LocalMapEventManager _eventManager;

		private void OnEnable()
		{
			_eventManager = FindObjectOfType<LocalMapEventManager>();
			_eventManager.OnMouseOnBounds.Add(MoveCamera);
		}

		private void OnDisable()
		{
			_eventManager.OnMouseOnBounds.Remove(MoveCamera);
		}

		private void MoveCamera(Vector3 direction)
		{
			direction.Set(direction.x, direction.z, direction.y);
			direction = Quaternion.Euler(0f, 45f, 0f) * direction; 
			transform.position += direction * Time.deltaTime * ScrollSpeed;
		}
	}
}
