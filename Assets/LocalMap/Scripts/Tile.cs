﻿using System;
using Common.Scripts;
using UnityEngine;

namespace LocalMap.Scripts
{
	public class Tile : MonoBehaviour {

		private LocalMapEventManager _eventManager;
		private int _indexX;

		public int IndexX
		{
			get { return _indexX; }
		}

		public int IndexY
		{
			get { return _indexY; }
		}

		public TileLight TileLight
		{
			get { return tileLight; }
		}

		private int _indexY;

		public TileLight tileLight;

		public Material EmptyMaterial;
		public Material ActiveMaterial;
		public Material BlockedMaterial;

		public Signal<Tile> OnMouseoverEnterSignal = new Signal<Tile>();
		public Signal<Tile> OnMouseoverExitSignal = new Signal<Tile>();
		public Signal<Tile> OnMouseClickedSignal = new Signal<Tile>();

		public TileSurface TileSurface { get; set; }
		public TileController TileController { get; set; }

		public void SetEventManager(LocalMapEventManager em)
		{
			_eventManager = em;
		}
	

		private void OnMouseEnter()
		{
			if (tileLight.IsPassable && TileController.Ground == TileSurface)
			{
				SetToActive();
			}
			else
			{
				SetToBlocked();
			}
			
			OnMouseoverEnterSignal.Dispatch(this);
		}

		private void OnMouseExit()
		{
			SetToEmpty();
			OnMouseoverExitSignal.Dispatch(this);
		}

		private void OnMouseDown()
		{
			if (tileLight.IsPassable && TileController.Ground == TileSurface)
			{
				OnMouseClickedSignal.Dispatch(this);
			}
		}


		private void SetToActive()
		{
			GetComponent<MeshRenderer>().material = ActiveMaterial;
		}
        
		private void SetToBlocked()
		{
            
			GetComponent<MeshRenderer>().material = BlockedMaterial;
		}

		private void SetToEmpty()
		{
            
			GetComponent<MeshRenderer>().material = EmptyMaterial;
		}

		public void SetIndices(int x, int y)
		{
			_indexX = x;
			_indexY = y;
		}

		public Vector2Int GetGridPosition()
		{
			return new Vector2Int(_indexX, _indexY);
		}

	}
}

