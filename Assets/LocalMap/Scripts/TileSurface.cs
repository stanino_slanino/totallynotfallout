﻿using System.Collections.Generic;
using UnityEngine;
using EpPathFinding.cs;

namespace LocalMap.Scripts
{
	public class TileSurface : MonoBehaviour
	{

		private MeshCollider _surface;
		private Tile[,] _tileGrid;

		public Tile[,] TileGrid
		{
			get { return _tileGrid; }
		}

		private TileLight[,] _tileLightGrid;

		public TileLight[,] TileLightGrid
		{
			get { return _tileLightGrid; }
		}

		private TileController _tileController;
		private const float Size = TileMetrics.size;

		private void OnEnable()
		{
			_surface = GetComponent<MeshCollider>();
			_tileController = FindObjectOfType<TileController>();
			PopulateSurfaceWithTiles();
		}

		public Vector2Int GetDimensions()
		{
			if (_surface == null)
			{
				_surface = GetComponent<MeshCollider>();
			}
			Vector3 size = _surface.bounds.size;
			int x = Mathf.FloorToInt(size.x/Size);
			int y = Mathf.FloorToInt(size.z/Size);
			return new Vector2Int(x, y);
		}

		public Vector3 GetStartingPoint()
		{
			if (_surface == null)
			{
				_surface = GetComponent<MeshCollider>();
			}
			Vector3 startPoint = _surface.bounds.min;
			startPoint.x += Size * 0.5f;
			startPoint.z += Size * 0.5f;
			startPoint.y += 0.1f;
			
			return startPoint;
		}
		
		private void PopulateSurfaceWithTiles()
		{
			Vector2Int dimensions = GetDimensions();
			_tileGrid = new Tile[dimensions.x, dimensions.y];
			_tileLightGrid = new TileLight[dimensions.x, dimensions.y];
			Vector3 start = GetStartingPoint();
			for (int x = 0; x < dimensions.x; x++)
			{
				for (int z = 0; z < dimensions.y; z++)
				{
					Vector3 position = start;
					position.x = start.x + x*TileMetrics.size;
					position.y = start.y;
					position.z = start.z + z * TileMetrics.size;
					
					Tile tile = _tileController.GetUnusedTile();
					
					TileLight tileLight = new TileLight(x, z, tile, this, position);

					tile.TileController = _tileController;
					tile.TileSurface = this;
					tile.tileLight = tileLight;
					tile.transform.position = position;
					tile.gameObject.SetActive(true);
					tile.SetIndices(x,z);
					_tileGrid[x, z] = tile;
					_tileLightGrid[x, z] = tileLight;
					
				}
			}
		}

		public List<TileLight> GetTileNeighborList(TileLight tileLight)
		{
			List<TileLight> result = new List<TileLight>();
			int x = tileLight.IndexX;
			int y = tileLight.IndexY;
			int startX = x - 1 < 0 ? 0 : x-1;
			int startY = y - 1 < 0 ? 0 : y-1;
			int maxX = _tileLightGrid.GetUpperBound(0);
			int maxY = _tileLightGrid.GetUpperBound(1);
			int endX = x + 1 > maxX ? maxX : x + 1;
			int endY = y + 1 > maxY ? maxY : y + 1;

			for (int xSquare = startX; xSquare <= endX; xSquare++)
			{
				for (int ySquare = startY; ySquare <= endY; ySquare++)
				{
					if (_tileLightGrid[xSquare, ySquare] != null)
					{
						result.Add(_tileLightGrid[xSquare, ySquare]);
					}			
				}	
			}
			return result;
		}

		public bool TestTilePassability(Vector3 tilePosition)
		{
			bool result = true;
			var colliders = Physics.OverlapBox(tilePosition + TileMetrics.PlayerOffset, TileMetrics.PlayerSize, Quaternion.identity);
			foreach (Collider collider in colliders)
			{
				if (collider.CompareTag("Obstacle"))
				{
					result = false;
				}
			}
			return result;
		}
	}
}
