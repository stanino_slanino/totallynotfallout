﻿//using UnityEngine;
//
//namespace LocalMap.Scripts._old
//{
//	public class HexMapSurface : MonoBehaviour
//	{
//		
//		float offsetX = 1.5f;
//		float offsetZ = 1.7f;
//
//		private MeshCollider _surface;
//
//		private void OnEnable()
//		{
//			_surface = GetComponent<MeshCollider>();
//		}
//
//		public Vector2Int GetDimensions()
//		{
//			Vector3 size = _surface.bounds.size;
//			int x = Mathf.FloorToInt(size.x/(HexMetrics.innerRadius*2f));
//			int y = Mathf.FloorToInt(size.z/(HexMetrics.outerRadius*1.5f));
//			return new Vector2Int(x, y);
//		}
//
//		public Vector3 GetStartingPoint()
//		{
//			Vector3 startPoint = _surface.bounds.min;
//			startPoint.x += offsetX;
//			startPoint.z += offsetZ;
//			startPoint.y += 0.1f;
//			
//			return startPoint;
//		}
//	}
//}
