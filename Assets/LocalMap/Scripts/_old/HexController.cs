﻿//using System.Collections.Generic;
//using UnityEngine;
//
//namespace LocalMap.Scripts._old
//{
//	public class HexController : MonoBehaviour
//	{
//
//		public Hex HexPrefab;
//		public HexMapSurface Ground;
//		
//		private LocalMapEventManager _eventManager;
//		[SerializeField]
//		private Hex[,] _hexGrid;
//		private Hex _lastSelectedHex;
//		
//		private const int maxSizeX = 64;
//		private const int maxSizeY = 64;
//	
//	
//		private void OnEnable()
//		{
//			_eventManager = FindObjectOfType<LocalMapEventManager>();
//			_eventManager.OnMouseOnHex.Add(EnableHex);
//			_eventManager.OnHexClicked.Add(PrintHexInfo);
//			CreateHexes();
//		}
//
//		private void PrintHexInfo(Hex hex)
//		{
//			List<Hex> neighbors = hex.GetHexNeighbors(_hexGrid);
//			Debug.Log(hex.GetGridPosition());
//			foreach (Hex neighbor in neighbors)
//			{
//				neighbor.SetToBlocked();
////				Debug.Log(neighbor.GetGridPosition());
//			}
//		}
//
//
//
//		private void OnDisable()
//		{
//			_eventManager.OnMouseOnHex.Remove(EnableHex);
//		}
//
//		private void EnableHex(Hex hex)
//		{
//			if (hex != _lastSelectedHex && _lastSelectedHex != null)
//			{
//				_lastSelectedHex.SetToEmpty();
//			}	
//			hex.SetToActive();
//			_lastSelectedHex = hex;
//		}
//
//		private void CreateHexes()
//		{
//			_hexGrid = new Hex[maxSizeX, maxSizeY];
//			Vector3 start = Ground.GetStartingPoint();
//			Vector2Int dimensions = Ground.GetDimensions();
//			for (int x = 0; x < dimensions.x; x++)
//			{
//				for (int z = 0; z < dimensions.y; z++)
//				{
//					Vector3 position = start;
//					position.x = start.x + (x - z * 0.5f + z / 2) * (HexMetrics.innerRadius * 2f);
//					position.y = start.y;
//					position.z = start.z + z * (HexMetrics.outerRadius * 1.5f);
//					
//					Hex hex = Instantiate(HexPrefab, position, Quaternion.Euler(90,0,0));
//					hex.SetIndices(x,z);
//					_hexGrid[x, z] = hex;
//				}
//			}
//		}
//
//		
//	}
//}
