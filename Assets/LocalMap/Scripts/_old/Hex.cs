﻿//using System.Collections.Generic;
//using UnityEngine;
//
//namespace LocalMap.Scripts._old
//{
//    public class Hex : MonoBehaviour
//    {
//
//        private HexController _hexController;
//        private int _indexX;
//        private int _indexY;
//
//        public Material EmptyMaterial;
//        public Material ActiveMaterial;
//        public Material BlockedMaterial;
//        
//        
//        private void OnEnable()
//        {
//            _hexController = FindObjectOfType<HexController>();
//        }
//
//        private void OnDisable()
//        {
//
//        }
//
//        public void SetToActive()
//        {
//            GetComponent<MeshRenderer>().material = ActiveMaterial;
//        }
//        
//        public void SetToBlocked()
//        {
//            
//            GetComponent<MeshRenderer>().material = BlockedMaterial;
//        }
//        
//        public void SetToEmpty()
//        {
//            
//            GetComponent<MeshRenderer>().material = EmptyMaterial;
//        }
//
//        public void SetIndices(int x, int y)
//        {
//            _indexX = x;
//            _indexY = y;
//        }
//
//        public Vector2Int GetGridPosition()
//        {
//            return new Vector2Int(_indexX, _indexY);
//        }
//
//        public List<Hex> GetHexNeighbors(Hex[,] grid)
//        {
//            int[,] neighborDirections =
//            {
//                {1, 0},
//                {0, 1},
//                {-1, 1},
//                {-1, -0},
//                {-1, -1},
//                {0, -1}
//            };
//
//            List<Hex> neighbors = new List<Hex>();
//
//            int maxX = grid.GetLength(0);
//            int maxY = grid.GetLength(1);
//
//            for (int i = 0; i < neighborDirections.GetLength(0); i++)
//            {
//                int xPos = _indexX + neighborDirections[i, 0];
//                int yPos = _indexY + neighborDirections[i, 1];
//                
//                if (xPos >= 0 && yPos >= 0 && xPos < maxX && yPos < maxY && grid[xPos, yPos] != null)
//                {
//                    neighbors.Add(grid[xPos, yPos]);
//                }
//            }
//            
//            return neighbors;
//        }
//    }
//}