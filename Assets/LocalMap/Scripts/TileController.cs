﻿using System.Collections.Generic;
using LocalMap.Scripts;
using UnityEngine;

public class TileController : MonoBehaviour {

	public Tile TilePrefab;
	public TileSurface Ground;
		
	public LocalMapEventManager _eventManager;
	private TileSurface[] _tileSurfaces;
	private Tile[,] _tileGrid;
	private Tile _lastSelectedHex;
	private Stack<Tile> _tileStack = new Stack<Tile>();
		
	private const int maxSizeX = 64;
	private const int maxSizeY = 64;
	
	
	private void OnEnable()
	{
		_eventManager.OnLadderClicked.Add(ChangeGround);
		_tileSurfaces = FindObjectsOfType<TileSurface>();
	}
	
	private void OnDisable()
	{
		_eventManager.OnLadderClicked.Remove(ChangeGround);
	}


	private void ChangeGround(Ladder ladder)
	{
		Ground = Ground == ladder.SurfaceLower ? ladder.SurfaceUpper : ladder.SurfaceLower;
	}

	private void DestroyTiles()
	{
		foreach (Tile tile in _tileGrid)
		{
			if (tile != null)
			{
				_tileStack.Push(tile);
				tile.gameObject.SetActive(false);
			}
		}
	}

	public Tile GetUnusedTile()
	{
		Tile tile = _tileStack.Count == 0 ? Instantiate(TilePrefab, Vector3.zero, Quaternion.Euler(90,0,0)) : _tileStack.Pop();
		tile.OnMouseClickedSignal.Add(_eventManager.TileClickedDispatcher);
		return tile;
	}
	
	
}

