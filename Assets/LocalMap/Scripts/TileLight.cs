﻿using System.Collections.Generic;
using UnityEngine;

namespace LocalMap.Scripts
{
    public class TileLight
    {



        private readonly int _indexX;
        private readonly int _indexY;
        private readonly TileSurface _tileSurface;
        private bool _isPassable;
        private readonly Vector3 _worldPosition;


        private List<TileLight> neighbors;

        private Tile _tile;

        #region GettersSetters

        public int IndexX
        {
            get { return _indexX; }
        }

        public int IndexY
        {
            get { return _indexY; }
        }

        public List<TileLight> Neighbors
        {
            get { return neighbors ?? (neighbors = _tileSurface.GetTileNeighborList(this)); }
        }

        public Tile Tile
        {
            get { return _tile; }
            set { _tile = value; }
        }
        
        public bool IsPassable
        {
            get { return _isPassable; }
        }
        
        public Vector3 WorldPosition
        {
            get { return _worldPosition; }
        }

        #endregion
    
        public TileLight(int x, int y, Tile tile, TileSurface tileSurface, Vector3 worldPosition)
        {
            _indexX = x;
            _indexY = y;
            _tileSurface = tileSurface;
            _tile = tile;
            _worldPosition = worldPosition;
            _isPassable = _tileSurface.TestTilePassability(_worldPosition);
        }

    }
}
