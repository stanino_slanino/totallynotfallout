﻿using UnityEngine;
using Common.Scripts;

namespace WorldMap.Scripts
{
	public class MapEventManager : MonoBehaviour
	{
		//editor properties
		public Camera Cam;
		
		public Signal<Vector3> OnMapClicked = new Signal<Vector3>();
		public Signal<Vector3> OnMouseAtMapBounds = new Signal<Vector3>();

		private void Update()
		{
			Vector3 mPosition = Input.mousePosition;
			Vector3 scrollVector = CommonCameraUtils.GetScrollVector(mPosition);

			if (scrollVector != Vector3.zero)
			{
				OnMouseAtMapBounds.Dispatch(scrollVector);
			}

			if (Input.GetMouseButtonDown(0))
			{
				Ray ray = Cam.ScreenPointToRay(mPosition);
				RaycastHit[] hit = Physics.RaycastAll(ray);
				foreach (RaycastHit h in hit)
					if (h.collider.name == "FalloutMap")
						if (OnMapClicked != null)
							OnMapClicked.Dispatch(h.point);
			}
		}
	}
}
