﻿using UnityEngine;
using UnityEngine.XR.WSA.WebCam;

namespace WorldMap.Scripts
{
	public class CameraController : MonoBehaviour
	{
		
		private const float ScrollSpeed = 3.0f;

		private Vector3 _nextPos;
		private PlayerController _player;
		private CameraMode _cameraMode = CameraMode.Following;
		private MapEventManager _mapEventManager;
		private enum CameraMode
		{
			Following,
			Free
		}

		// Use this for initialization
		private void Start () {
			_player = FindObjectOfType<PlayerController>();
			
		}

		private void OnEnable()
		{
			_mapEventManager = FindObjectOfType<MapEventManager>();
			_mapEventManager.OnMapClicked.Add(SetFollowingCamera);
			_mapEventManager.OnMouseAtMapBounds.Add(SetFreeCamera);
		}

		private void OnDisable()
		{
			_mapEventManager.OnMapClicked.Remove(SetFollowingCamera);
			_mapEventManager.OnMouseAtMapBounds.Remove(SetFreeCamera);
		}

		// Update is called once per frame
		private void Update ()
		{
			if (_player == null)
				_player = FindObjectOfType<PlayerController>();
			MoveWorldCamera();
		}

		private void MoveWorldCamera()
		{
			Vector3 pos;
			if (_cameraMode == CameraMode.Following)
				pos = new Vector3(
//				Mathf.Clamp(_player.transform.position.x, -21f, 21f),
//				Mathf.Clamp(_player.transform.position.y, -25f, 25f),
					_player.transform.position.x,
					_player.transform.position.y,
					transform.position.z);
			else
				pos = _nextPos;

			pos.x = Mathf.Clamp(pos.x, -21f, 21f);
			pos.y = Mathf.Clamp(pos.y, -25f, 25f);

			transform.position = pos;
		}

		private void SetFreeCamera(Vector3 direction)
		{
			_cameraMode = CameraMode.Free;
			_nextPos = transform.position + (direction * Time.deltaTime * ScrollSpeed);
		}

		private void SetFollowingCamera(Vector3 destination)
		{
			MoveWorldCamera();
			_cameraMode = CameraMode.Following;
		}
	}
}
