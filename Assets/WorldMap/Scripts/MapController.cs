﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace WorldMap.Scripts
{
	public class MapController : MonoBehaviour
	{
		//prefabs
		public GameObject PlayerPrefab;
		public SquareController SquareController;
		public GameObject TargetPrefab;

		//private fields
		private GameObject _player;
		private GameObject _target;
		private MapEventManager _mapEventManager;
		private readonly SquareController [ , ] _squareGrid = new SquareController[30, 28];

		// Use this for initialization
		private void Start () {
			_player = CreatePlayer();
			_player.transform.parent = this.transform;
			CreateUnexploredSquares(_player.transform.position);
		}

		private void OnEnable()
		{
			_mapEventManager = GetComponent<MapEventManager>();
			_mapEventManager.OnMapClicked.Add(SetNewTarget);
		}

		private void OnDisable()
		{
			_mapEventManager.OnMapClicked.Remove(SetNewTarget);
		}

		private void SetNewTarget(Vector3 destination)
		{
			if (_target != null)
				Destroy(_target);
			_target = Instantiate(TargetPrefab, destination, Quaternion.identity);
		}

		private void CreateUnexploredSquares( Vector3 startPosition )
		{
			const int xSize = 30;
			const int ySize = 28;
			const float initialX = -27f;
			const float initialY = -29f;
			float curX = initialX;
			float curY = initialY;
			const float offsetX = 2.00f;
			const float offsetY = 2.00f;
		
			for (int x = 0; x < xSize; x++)
			{
				for (int y = 0; y < ySize; y++)
				{
					Vector3 pos = new Vector3(curX, curY, -0.01f);
				
					SquareController newSquare = Instantiate(SquareController, pos, Quaternion.identity);
					if (newSquare != null)
					{
						_squareGrid[x, y] = newSquare;
						newSquare.SetIndices(x, y);
						newSquare.SetMapController(this);
					}
				
					curX += offsetX;
				}
				curX = initialX;
				curY += offsetY;
			}

		}
	
		private GameObject CreatePlayer ()
		{
			Vector3 camPosition = GameObject.Find("Main Camera").transform.position;
			camPosition.z = -1.0f;
			return Instantiate(PlayerPrefab, camPosition, Quaternion.identity);
		}

		public IEnumerable<SquareController> FindNeighbors(int x, int y)
		{
			List<SquareController> result = new List<SquareController>();
			int startX =  x - 1 < 0 ? 0 : x-1;
			int startY = y - 1 < 0 ? 0 : y-1;
			int maxX = _squareGrid.GetUpperBound(0);
			int maxY = _squareGrid.GetUpperBound(1);
			int endX = x + 1 > maxX ? maxX : x + 1;
			int endY = y + 1 > maxY ? maxY : y + 1;

			for (int xSquare = startX; xSquare <= endX; xSquare++)
				for (int ySquare = startY; ySquare <= endY; ySquare++)
					if (_squareGrid[xSquare, ySquare] != null)
						result.Add(_squareGrid[xSquare, ySquare]);
			return result;
		}
	}
}
