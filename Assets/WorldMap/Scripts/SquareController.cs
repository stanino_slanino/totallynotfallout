﻿using System.Collections.Generic;
using UnityEngine;

namespace WorldMap.Scripts
{
	public class SquareController : MonoBehaviour

	{
		private int _indexX;
		private int _indexY;
		private MapController _mapController;

		private void DestroySquare()
		{
			Destroy(gameObject);
			IEnumerable<SquareController> neighbors = _mapController.FindNeighbors(_indexX, _indexY);
			foreach (SquareController neighbor in neighbors)
				neighbor.ChangeToFog();
		}

		private void ChangeToFog()
		{
			SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
			spriteRenderer.sprite = Resources.Load<Sprite>("fog");
		}

		private void OnTriggerEnter2D(Collider2D other)
		{
			DestroySquare();
		}

		public void SetIndices(int x, int y)
		{
			_indexX = x;
			_indexY = y;
		}

		public void SetMapController(MapController mapController)
		{
			_mapController = mapController;
		}
	}
}
