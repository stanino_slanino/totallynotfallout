﻿using UnityEngine;

namespace WorldMap.Scripts
{
	public class PlayerController : MonoBehaviour
	{

		public GameObject TargetPointer;
		public float Speed;
		private Vector3 _destination;
		private MapEventManager _mapEventManager;

		private void Start()
		{
			_destination = transform.position;
		}

		private void OnEnable()
		{
			_mapEventManager = FindObjectOfType<MapEventManager>();
			_mapEventManager.OnMapClicked.Add(SetNewDestination);
		}

		private void OnDisable()
		{
			_mapEventManager.OnMapClicked.Remove(SetNewDestination);
		}

		private void Update () {
			float step = Speed * Time.deltaTime;
			Vector3 current = Vector3.MoveTowards(transform.position, _destination, step);
			transform.position = current;
		}

		private void SetNewDestination(Vector3 destination)
		{
			_destination = destination;
		}
	}
}
